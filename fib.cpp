#include <iostream>
#include <vector>

using namespace std;

unsigned long fibR(unsigned n)//recursive fibonacci implementation
{
  
  if (n <= 1) 
        return n; 
    return fib(n-1) + fib(n-2);
  
}



unsigned long fibDP(unsigned n)//dynamic programming fibonacci implementation
{
  
  /* Declare an array to store Fibonacci numbers. */
  int f[n+2];   // 1 extra to handle case, n = 0 
  int i; 
  
  /* 0th and 1st number of the series are 0 and 1*/
  f[0] = 0; 
  f[1] = 1; 
  
  for (i = 2; i <= n; i++) 
  { 
      /* Add the previous 2 numbers in the series 
         and store it */
      f[i] = f[i-1] + f[i-2]; 
  } 
  
  return f[n]; 
}
  
}


int main()
{

  unsigned input;

  
   cerr<<"Welcome to \"Fibonacci Comparison Program\". We first need some input :  ";

    cin>>input;

    cerr<<endl<<"fibDP("<<input<<") =  "<<fibDP(input)<<endl;

    cerr<<endl<<"fibR("<<input<<") =  "<<fibR(input)<<endl;

    return 0;
}
